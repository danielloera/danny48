import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.Timer;
import javax.swing.JPanel;

public class GamePanel extends JPanel implements ActionListener{
    public static Timer timer;
    public ArrayList<NumberPeg> moving, stationary;

    public GamePanel() {
        setSize(850,850);
        timer = new Timer(1, this);
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        moving = new ArrayList();
        stationary = new ArrayList();
        g.setColor(Color.gray);
        g.fillRect(0, 0 ,850,850);
        Graphics2D g2 = (Graphics2D) g;

        g.setFont(new Font("TimesNewRoman",Font.BOLD,64));
        for(NumberPeg p: Danny48.pegs) {
            if(p.doneAnimating()) {
                 stationary.add(p);
            } else {
                moving.add(p);
            }
        }
        //draw stationary Pegs
        for(NumberPeg p : stationary) {
            g.setColor(getPegColor(p));
            Rectangle rec = new Rectangle(p.currentX, p.currentY, 190, 190);
            g2.fill(rec);
            g.setColor(Color.black);
            paintNumber(g,p);
            //draw blank pegs
            for(int r = 0; r < 4; r++) {
                for(int c = 0; c < 4; c++) {
                    if(Danny48.board[r][c] == 0) {
                        g2.setColor(Color.LIGHT_GRAY);
                        g2.fill(new Rectangle(c * 200 + 10,r * 200 + 10, 190, 190));
                    }
                }
            }
        }
        for(NumberPeg p : moving) {
            if(!p.doneAnimating()) {
                g2.setColor(Color.LIGHT_GRAY);
                g2.fill(new Rectangle(p.finalX, p.finalY, 190, 190));
            }
       }
        if(!moving.isEmpty()) {
            if(!timer.isRunning()) {
                timer.start();
            }
            for(int m = 0; m < moving.size(); m++) {
                if(moving.get(m).doneAnimating()){
                   g2.setColor(getPegColor(moving.get(m)));
                   g2.fill(new Rectangle(moving.get(m).finalX, moving.get(m).finalY, 190, 190));
                   moving.remove(m);
                } else {
                    if(moving.get(m).doubled) {
                        NumberPeg temp = new NumberPeg(moving.get(m).getNum() / 2, moving.get(m).getRow(), moving.get(m).getCol());
                        temp.currentX = moving.get(m).finalX;
                        temp.currentY = moving.get(m).finalY;
                        g.setColor(getPegColor(temp));
                        g.fillRect(temp.currentX, temp.currentY, 190, 190);
                        paintNumber(g,temp);
                        g.setColor(getPegColor(temp));
                        temp.currentX = moving.get(m).currentX;
                        temp.currentY = moving.get(m).currentY;
                        g.fillRect(temp.currentX, temp.currentY, 190, 190);
                        paintNumber(g,temp);
                    } else {
                        g.setColor(getPegColor(moving.get(m)));
                        g2.fill(new Rectangle(moving.get(m).currentX, moving.get(m).currentY, 190, 190));
                        paintNumber(g,moving.get(m));
                    }
                }
            }
        } else {
            if(Danny48.shifted) {
               Danny48.addRandomPeg();
            }
            timer.stop();
            finalPaint(g);
        }
    }

    public void finalPaint(Graphics g) {
        for(int p = 0; p < Danny48.pegs.size(); p++) {
            g.setColor(getPegColor(Danny48.pegs.get(p)));
            g.fillRect(Danny48.pegs.get(p).finalX, Danny48.pegs.get(p).finalY, 190, 190);
            g.setColor(Color.black);
            paintNumber(g,Danny48.pegs.get(p));
        }
    }

    public void paintNumber(Graphics g,NumberPeg p) {
        int num = p.getNum();
        g.setColor(Color.black);
        if(num < 10) {
            g.drawString(Integer.toString(num),p.currentX + 70,p.currentY + 115);
        }
        else if(num < 100) {
            g.drawString(Integer.toString(num),p.currentX+ 60,p.currentY + 115);
        }
        else if(num < 1000) {
            g.drawString(Integer.toString(num),p.currentX + 42,p.currentY  + 115);
        }
        else {
           g.drawString(Integer.toString(num),p.currentX + 27,p.currentY  + 115);
        }
    }

    public Color getPegColor(NumberPeg findColor) {
        switch(findColor.getNum()) {
            case 2:
            case 4:
            case 8:
                return new Color(219, 222, 124);
            case 16:
                return new Color(245, 217, 93);
            case 32:
                return new Color(240, 205, 48);
            case 64:
                return new Color(255, 208, 0);
            case 128:
                return new Color(255, 166, 0);
            case 256:
                return new Color(255, 145, 0);
            case 512:
                return new Color(255, 106, 0);
            case 1024:
                return new Color(255, 89, 0);
            case 2048:
                return new Color(255, 25, 0);
            default:
                return new Color(35, 217, 98);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        for(NumberPeg p : moving) {
            if(!p.doneAnimating()) {
                p.step();
            }
            repaint();
        }
    }
}
