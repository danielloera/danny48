public class NumberPeg {
    private int number, row, col;
    public int initialX, initialY, finalX, finalY, currentX, currentY;
    public boolean doubled;

    public NumberPeg(int n, int r, int c) {
        number = n;
        row = r;
        col = c;
        doubled = false;
    }

    public int getNum() {
        return number;
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    public void doubleNum() {
        number *= 2;
    }

    public void incRow() {
        row++;
    }

    public void incCol() {
        col++;
    }

    public void decCol() {
        col--;
    }

    public void decRow() {
        row--;
    }

    public void setInitials() {
        initialX = (col * 200) + 10;
        initialY = (row * 200) + 10;
        currentX = initialX;
        currentY = initialY;
    }

    public void setFinals() {
        finalX =  (col * 200) + 10;
        finalY =  (row * 200) + 10;
    }

    public boolean doneAnimating() {
        return ((finalX == initialX && finalY == initialY) || (currentX == finalX && currentY == finalY));
    }

    public void step() {
        if (currentX < finalX) {
            currentX += 20;
        }
        else if (currentX > finalX) {
            currentX -= 20;
        }
        if (currentY < finalY){
            currentY += 20;
        }
        else if (currentY > finalY) {
            currentY -= 20;
        }
    }
}
