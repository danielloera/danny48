import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Danny48 {
    static boolean shifted;
    static NumberPeg foundPeg;
    static GamePanel panel;
    static JFrame frame;
    static int row,col;
    static int[][] board;
    static ArrayList<Integer> availableSpots;
    static ArrayList<NumberPeg> pegs;

    public static void main(String[] args) {

        class ArrowListener implements KeyListener {

            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
                if(!GamePanel.timer.isRunning()) {
                    if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
                        shift("right");
                    }
                    else if(e.getKeyCode() == KeyEvent.VK_LEFT) {
                        shift("left");
                    }
                    else if(e.getKeyCode() == KeyEvent.VK_UP) {
                        shift("up");
                    }
                    else if(e.getKeyCode() == KeyEvent.VK_DOWN) {
                        shift("down");
                    }
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        }

        board = new int[][] {{0,0,0,0},
                             {0,0,0,0},
                             {0,0,0,0},
                             {0,0,0,0},};
        availableSpots = new ArrayList(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16));
        pegs = new ArrayList();
        frame  = new JFrame("Danny48");
        frame.setSize(820, 850);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.addKeyListener(new ArrowListener());
        frame.setLocationRelativeTo(null);
        panel = new GamePanel();
        frame.add(panel);
        start();
    }
    public static void start(){
        addRandomPeg();
        addRandomPeg();
        printBoard();
    }

    public static void shift(String direction){
        shifted = false; //check if the board has shifted
        System.out.println(direction);
        updateBoard();
        switch(direction){
        case "right":
            for(int s = 3; s >= 0; s--) {//reading from column right to left.
                for(int p = 0; p < pegs.size(); p++) {//scanning through all number pegs
                    if(pegs.get(p).getCol() == s) {//making sure the column is correct
                        pegs.get(p).setInitials();//set the initial X and Y coordinates.
                        pegs.get(p).doubled = false;//set doubled variable to false.
                        while(pegs.get(p).getCol() + 1 <= 3){//doesnt go out of bounds
                            if(pegLocated(direction,pegs.get(p))){//check if a peg is located to the right
                                if(foundPeg.getNum() == pegs.get(p).getNum()){//if the peg to the right has the same number
                                    pegs.get(p).doubleNum();//couble current peg
                                    pegs.get(p).doubled = true;//set doubled variable to true
                                    pegs.get(p).incCol();//increase the column
                                    pegs.get(p).setFinals();//set final X and Y coordinates
                                    pegs.remove(foundPeg);//remove the previous peg
                                    shifted = true;//tell the game the board has shifted
                                    updateBoard();//update the matrix
                                    break;//
                                }
                                updateBoard();
                                break;
                            } else {
                                pegs.get(p).incCol();//increase column
                                pegs.get(p).setFinals();//set final X and Y coordinates
                                shifted = true;//board has shifted
                                updateBoard();//update matrix
                            }

                        }

                    }
                }
            }
            break;
        case "left":
            for(int s = 0; s <= 3; s++) {
                for(int p = 0; p < pegs.size(); p++) {//scan through all pegs
                    if(pegs.get(p).getCol() == s) {
                        pegs.get(p).setInitials();
                        pegs.get(p).doubled = false;
                        while(pegs.get(p).getCol() - 1 >= 0) {//doesnt go out of bounds
                            if(pegLocated(direction,pegs.get(p))) {//check if a peg is located to the right
                                if(foundPeg.getNum() == pegs.get(p).getNum()) {//if the peg to the right has the same number
                                    pegs.get(p).doubled = true;
                                    pegs.get(p).doubleNum();
                                    pegs.get(p).decCol();
                                    pegs.get(p).setFinals();
                                    pegs.remove(foundPeg);//double the peg to the right
                                    shifted = true;
                                    updateBoard();
                                    break;//remove the current peg
                                }
                                updateBoard();
                                break;
                            } else {
                                pegs.get(p).decCol();
                                pegs.get(p).setFinals();
                                shifted = true;
                                updateBoard();
                            }
                        }
                    }
                }
            }
            break;
        case "up":
            for(int s = 0; s <= 3; s++) {
                for(int p = 0; p < pegs.size(); p++){//scan through all pegs
                    if(pegs.get(p).getRow() == s){
                        pegs.get(p).setInitials();
                        pegs.get(p).doubled = false;
                        while(pegs.get(p).getRow() - 1 >= 0) {//doesnt go out of bounds
                            if(pegLocated(direction,pegs.get(p))) {//check if a peg is located to the right
                                if(foundPeg.getNum() == pegs.get(p).getNum()) {//if the peg to the right has the same number
                                    pegs.get(p).doubled = true;
                                    pegs.get(p).doubleNum();
                                    pegs.get(p).decRow();
                                    pegs.get(p).setFinals();
                                    pegs.remove(foundPeg);//double the peg to the right
                                    shifted = true;
                                    updateBoard();
                                    break;//remove the current peg
                                }
                                updateBoard();
                                break;
                            }else{
                                pegs.get(p).decRow();
                                pegs.get(p).setFinals();
                                shifted = true;
                                updateBoard();
                            }

                        }
                    }
                }
            }
            break;
         case "down":
            for(int s = 3;s >= 0;s--) {
                for(int p = 0; p < pegs.size(); p++) {//scan through all pegs
                    if(pegs.get(p).getRow() == s){
                        pegs.get(p).setInitials();
                        pegs.get(p).doubled = false;
                        while(pegs.get(p).getRow() + 1 <= 3) {//doesnt go out of bounds
                            if(pegLocated(direction, pegs.get(p))) {//check if a peg is located to the right
                                if(foundPeg.getNum() == pegs.get(p).getNum()) {//if the peg to the right has the same number
                                    pegs.get(p).doubled = true;
                                    pegs.get(p).doubleNum();
                                    pegs.get(p).incRow();
                                    pegs.get(p).setFinals();
                                    pegs.remove(foundPeg);//double the peg to the right
                                    shifted = true;
                                    updateBoard();
                                    break;//remove the current peg
                                }
                                updateBoard();
                                break;
                            } else {
                                pegs.get(p).incRow();
                                pegs.get(p).setFinals();
                                shifted = true;
                                updateBoard();
                            }
                        }
                    }
                }
            }
        }
        if(shifted == false && availableSpots.isEmpty()) {
            lose();
        }
        printBoard();
    }

    public static boolean pegLocated(String loc, NumberPeg find){
        switch(loc) {
            case"right":
                for(NumberPeg p : pegs){
                    if(p.getCol() == find.getCol() + 1 && p.getRow() == find.getRow()) {
                        foundPeg = p;
                        return true;
                    }
                }
            break;
            case "left":
                for(NumberPeg p : pegs) {
                    if(p.getCol() == find.getCol() - 1 && p.getRow() == find.getRow()) {
                        foundPeg = p;
                        return true;
                    }
                }
            break;
            case "up":
                for(NumberPeg p : pegs) {
                    if(p.getCol() == find.getCol() && p.getRow() == find.getRow() - 1) {
                        foundPeg = p;
                        return true;
                    }
                }
            break;
            case "down":
                for(NumberPeg p : pegs) {
                    if(p.getCol() == find.getCol() && p.getRow() == find.getRow() + 1){
                        foundPeg = p;
                        return true;
                    }
                }
            break;
        }
        return false;
    }

    public static void addRandomPeg() {
        int choose = (int)(Math.random() * 10);
        int ranInt;
        if(choose >= 2) {
            ranInt = 2;
        }
        else {
            ranInt = 4;
        }
        getRandomSpot();
        NumberPeg randomPeg = new NumberPeg(ranInt, row, col);
        randomPeg.setInitials();
        randomPeg.setFinals();
        pegs.add(randomPeg);
        updateBoard();
    }

    public static void getRandomSpot(){
        int ran = (int)(Math.random() * availableSpots.size());
        gridTranslate(availableSpots.get(ran));
    }

    public static void gridTranslate(int trans){
        if(trans == 1 || trans == 2 || trans == 3 || trans == 4) {
            row = 0;
            col = trans - 1;
        } else if (trans == 5 || trans == 6 || trans == 7 || trans == 8) {
            row = 1;
            col = trans - 5;
        } else if (trans == 9 || trans == 10 || trans == 11 || trans == 12) {
            row = 2;
            col = trans - 9;
        } else {
            row = 3;
            col = trans - 13;
        }
    }

    public static void updateBoard(){
        board = new int[][] {{0,0,0,0},
                             {0,0,0,0},
                             {0,0,0,0},
                             {0,0,0,0},};
        availableSpots.clear();
        for(NumberPeg p : pegs) {
            board[p.getRow()][p.getCol()] = p.getNum();
        }
        int c = 1;
        for(int x = 0; x < 4; x++) {
            for(int y = 0; y < 4; y++) {
                if(board[x][y] == 0){
                    availableSpots.add(c);
                }
                c++;
            }
        }
    }

    public static void printBoard(){
        System.out.println();
        for(int a = 0; a < 4; a++) {
            for(int b = 0; b < 4; b++) {
               System.out.printf("%-4s", board[a][b] + "");
            }
            System.out.println();
        }
        panel.repaint();
    }

    public static void lose(){
        frame.dispose();
        JFrame lose = new JFrame("YOU LOSE!");
        lose.setSize(300, 300);
        lose.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        lose.setVisible(true);
        lose.setLocationRelativeTo(null);
        lose.setLayout(new BorderLayout());
        JLabel label = new JLabel("YOU LOSE!");
        label.setFont(new Font("TimeNewRoman", Font.BOLD, 40));
        lose.add(label, BorderLayout.CENTER);
    }
}
